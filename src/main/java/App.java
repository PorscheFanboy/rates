import java.util.*;
import java.util.regex.Pattern;
import java.io.File;


public class App 
{
	private Map<String, Double> pairs;
	
	public Double getFXQuote(String curA, String curB) throws Exception {
		String s = curA + "," + curB;
		if (pairs.containsKey(s)) {
			return pairs.get(s);
		} else {
			System.out.println("No such currency pair");
			throw new Exception();
		}
	}
	
	
	App(String fileName) {
		pairs = new HashMap<String, Double>();
		parseInput(fileName);
	}
	
	
	private void parseInput(String fileName) {
		File file = new File(fileName);
    	Scanner sc = new Scanner(System.in);
    	try {
    		sc = new Scanner(file);

    	} catch (Exception e) {
    		System.out.println("File not found");
    		return;
    	}
    	sc.next();
    	while (sc.hasNext()) {
    		String line = sc.next();

    		String[] arr = line.split(",");
    		pairs.put(arr[0] + "," + arr[1], Double.parseDouble(arr[2]));
    		
    		
    	}
	}
	
	

	
    public static void main( String[] args )
    {
    	App a = new App("rates.csv");
    	try {
    		System.out.println(a.getFXQuote("USD", "CAD"));
    	} catch (Exception e) {}
    	try {
    		System.out.println(a.getFXQuote("EUR", "MMK"));
    	} catch (Exception e) {}
    	
        System.out.println( "Hello World!" );
    }
}
