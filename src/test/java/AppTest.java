
import static org.junit.Assert.assertEquals;

import org.junit.*;
import org.junit.rules.ExpectedException;

/**
 * Unit test for simple App.
 */
public class AppTest {
	public final ExpectedException exception = ExpectedException.none();

    
    @Test
    public void testQuotation() {
    	App app = new App("rates.csv");
    	try {
			assertEquals(app.getFXQuote("USD", "CAD"), 0.9925, 0.0001);
			assertEquals(app.getFXQuote("EUR", "MMK"), 8.439, 0.0001);
		} catch (Exception e) {
			
			e.printStackTrace();
		}

    }
    
    @Test (expected = Exception.class)
    public void testException() throws Exception {
    	App app = new App("rates.csv");

    	app.getFXQuote("QWER", "qwer");

    	
    	exception.expect(Exception.class);
    	
    	
    }
}
